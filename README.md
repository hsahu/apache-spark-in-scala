<div style="text-align:left"><img width="150" src ="images/Apache-Spark-Logo.jpeg" alt="Apache Spark"/></div>
<p></p>

1. Fast and general engine for large data processing.<br/>
2. Distributed processing of large data sets.<br/>
3. Cluster computing framework.<br/>
4. Built on top of Hadoop MapReduce.<br/>
5. It extends the MapReduce model to efficiently use more types of computations which includes **stream processing**, **interactive queries**, **batch applications** and **iterative algorithm**.<br/>
6. Spark uses Hadoop in two ways – one is storage and second is processing.<br/>
7. Spark uses **in-memory cluster computing** that increases the processing speed of an application.<br/>
8. Built around one concept - **RDD (Resilient Distributed Datasets)**

<div style="text-align:center"><img width="400" src ="images/spark_engine.jpg" alt="Spark Engine"/></div>

#### Apache Spark Features

1. **Speed** Run Programs up to 100x times faster than Hadoop MapReduce in memory and 10x times faster on disk (possible by reducing number of r/w operations to disk). It stores the intermediate processing data in memory.

<div style="text-align:center"><img width="400" src ="images/spark-low-latency.png" alt="sparkcontext low latency"/></div>

2. **DAG (Directed Acyclic Graph)** optimizes workflows.

3. **Advanced Analytics** Spark not only supports ‘Map’ and ‘reduce’. It also supports SQL queries, Streaming data, Machine learning (ML), and Graph algorithms.

#### Spark Built on Hadoop

**Standalone** : Spark Standalone deployment means Spark occupies the place on top of HDFS(Hadoop Distributed File System) and space is allocated for HDFS, explicitly. Here, Spark and MapReduce will run side by side to cover all spark jobs on cluster.

**Hadoop Yarn** : Hadoop Yarn deployment means, simply, spark runs on Yarn (cluster management technology). It helps to integrate Spark into Hadoop ecosystem. It allows other components to run on top of stack.

**Spark in MapReduce (SIMR)** : Spark in MapReduce is used to launch spark job in addition to standalone deployment. With SIMR, user can start Spark and uses its shell without any administrative access.

------------

### Resilient Distributed Datasets (RDD)

**Resilient** : If one node goes down in your cluster it can still recover from that and pickup from where it left off.

**Distributed** : Splits up your data and process across nodes in cluster.

1. Fundamental data structure of Spark.
2. Immutable distributed collection of objects.
3. Each dataset in RDD is divided into logical partitions which may be computed on different nodes of the cluster.
4. RDD can contain any type of objects, including user-defined classes.
8. RDD is a fault-tolerant.

### Spark Context

1. Created by driver program.
2. Responsible for making RDD *resilient* and *distributed*.
3. RDD can also be created from JDBC, Elastic Search, HBase, Cassandra.
4. Load up date from Amazon S3, HDFS.

<div style="text-align:center"><img width="400" src ="images/spark-architecture.png" alt="Spark Architecture"/></div>

### Transforming RDD's

**map()** apply a function in every row of RDD.

**flatmap()** is similar to map, but each input item can be mapped to 0 or more output items. flatmap can generate many lines from one input line or zero lines.

**filter()** trim down unnecessary data from RDD.

**distinct()** remove duplicate rows from RDD.

**sample()** creates a random sample RDD from input RDD.

**union(), intersection(), subtract(), cartesian()** Perform set operations on RDD between any two RDD's.

All these functions doesn't modify original RDD because it is immutable.

### RDD Actions

**collect()** takes the result of an RDD and passes back to driver script.

**count()** get a count of how many rows are there in RDD.

**countByValue()** returns a Map containing <unique value, count> where *count* is a integer describing how many times each unique value appears.

**take()** returns first k result in RDD.

**reduce()** this actually combine together all the different value associated with a given value (similar to reduce in Hadoop).

**aggregate()** this function allows the user to apply two different reduce functions to the RDD. The first reduce function is applied within each partition to reduce the data within each partition into a single result. The second reduce function is used to combine the different reduced results of all partitions together to arrive at one final result.
* The initial value is applied at both levels of reduce. So both at the intra partition reduction and across partition reduction.
* Both reduce functions have to be commutative and associative (so that final result will be same irrespective of execution order).

```scala
// average of all the numbers
// accumulator is tuple contains sum of elements and number of elements
val result = input.aggregate((0, 0))(
  (acc, value) => (acc._1 + value, acc._2 + 1),
  (acc1, acc2) => (acc1._1 + acc2._1, acc1._2 + acc2._2))
val avg = result._1 / result._2.toDouble
```

**In spark, nothing actually happens until you call an action on RDD. When you call any action, it will go and figure out what's the best optimal path for producing these results. At that point spark construct *Directed Acyclic Graph*, execute it in most optimal manner on cluster.**

### Spark Internals
Step by step description of how spark works internally....
1. A job is broken into stages based on when data needs to be reorganized.
2. Each stage is broken into tasks which may be distributed across a cluster.
3. Finally the task are scheduled across your cluster and executed.


### Key/Value RDD's
Key value RDDs are same as general RDD but it contains data in key value pairs instead of rows. It is just a map pairs of data into the RDD using tuples.You can create pair RDD something like this...

`val keyValuePairRDD = originalRDD.map(x=> (x,1))`

Ok to have tuples or other objects as value as well.

If RDD contains tuples and tuple contains two objects then spark automatically treat that RDD as key/value RDD.

**reduceByKey()** combine value with the same key using some function.

`rdd.reduceByKey((x, y) => x+y)` it adds up all the values of the same key and return a new RDD.

**groupByKey()** - Group value with the same key and returns `(key, List of values of same key)`.

**sortByKey()** - Sort an RDD by key.

**keys()** - Create an RDD of just keys.

**values()** - Create an RDD of just values.

**join, rightOuterJoin, leftOuterJoin, cogroup, subtractByKey** - Create an RDD by doing some SQL style joins on two key/value RDD.

**combineByKey()** similar to aggregate() but worked on key value paired RDD. Here,

* *1st Argument* : createCombiner is called when a key(in the RDD element) is found for the first time in a given Partition. This method creates an initial value for the accumulator for that key.
* *2nd Argument* : mergeValue is called when the key already has an accumulator.
* *3rd Argument* : mergeCombiners is called when more that one partition has accumulator for the same key.

```scala
scala> val inputrdd = sc.parallelize(Seq(
     |                    ("maths", 50), ("maths", 60),
     |                    ("english", 65),
     |                    ("physics", 66), ("physics", 61), ("physics", 87)),
     |                    1)
inputrdd: org.apache.spark.rdd.RDD[(String, Int)] = ParallelCollectionRDD[41] at parallelize at <console>:27

scala> inputrdd.getNumPartitions                      
res1: Int = 1

scala> val reduced = inputrdd.combineByKey(
     |     (mark) => {
     |       println(s"Create combiner -> ${mark}")
     |       (mark, 1)
     |     },
     |     (acc: (Int, Int), v) => {
     |       println(s"""Merge value : (${acc._1} + ${v}, ${acc._2} + 1)""")
     |       (acc._1 + v, acc._2 + 1)
     |     },
     |     (acc1: (Int, Int), acc2: (Int, Int)) => {
     |       println(s"""Merge Combiner : (${acc1._1} + ${acc2._1}, ${acc1._2} + ${acc2._2})""")
     |       (acc1._1 + acc2._1, acc1._2 + acc2._2)
     |     }
     | )
reduced: org.apache.spark.rdd.RDD[(String, (Int, Int))] = ShuffledRDD[42] at combineByKey at <console>:29

scala> reduced.collect()
Create combiner -> 50
Merge value : (50 + 60, 1 + 1)
Create combiner -> 65
Create combiner -> 66
Merge value : (66 + 61, 1 + 1)
Merge value : (127 + 87, 2 + 1)
res2: Array[(String, (Int, Int))] = Array((maths,(110,2)), (physics,(214,3)), (english,(65,1)))

scala> val result = reduced.mapValues(x => x._1 / x._2.toFloat)
result: org.apache.spark.rdd.RDD[(String, Float)] = MapPartitionsRDD[43] at mapValues at <console>:31

scala> result.collect()
res3: Array[(String, Float)] = Array((maths,55.0), (physics,71.333336), (english,65.0))

```

`With key/value data, use mapValues() and flatMapValues() if your transformation doesn't affect the keys. It's more efficient.`

### Broadcast Variables
Broadcast variables allow the programmer to keep a read-only variable cached on each machine rather than shipping a copy of it with tasks.

Explicitly creating broadcast variables is only useful when tasks across multiple stages need the same data.

`sparkContext.broadcast()` : Broadcast objects to executors, such that they are always there whenever needed.

<div style="text-align:center"><img width="400" src ="images/sparkcontext-broadcast-executors.png" alt="sparkcontext broadcast executors"/></div>

### Shared Variables: Accumulators
An accumulator is a shared variable across entire spark cluster. It allows many executors to only add something in a shared variable. Spark natively supports accumulators of numeric types.

An accumulator is created from an initial value v by calling `val accumulator =  SparkContext.accumulator(v)`. Tasks running on the cluster can then add to it using the add method or the += operator.

```scala
  scala> val accumulator = sc.accumulator(0)
  scala> sc.parallelize(Array(1, 2, 3)).foreach(x => accumulator += x)
```

Tasks running on the cluster cannot read accumulator's value. Only the driver program can read the accumulator’s value by using `accumulator.value()`.


### Partition and Partitioning

Partitioning is nothing but dividing dataset into parts. In distributed system, we can define it as the division of the large dataset and store them as multiple parts across the cluster.

* Spark works on data locality principle. Worker nodes takes the data for processing. By doing partitioning network I/O will be reduced so that data can be processed a lot faster.

* In Spark, operations like `co-group`, `groupBy`, `groupByKey` and many more will need lots of I/O operations. In this scenario, if we apply partitioning, then we can reduce the number of I/O operations rapidly so that we can speed up the data processing.

* RDD is split into multiple partitions which may be computed on different nodes of the cluster. In Spark, every function is performed on RDDs only.

* Make sure you always have as least as many partitions as number of executors that fit within available memory.

* use **.partitionBy()** on an RDD before running a large operation (join(), coGroup(), groupWith(), leftOuterJoin(), rightOuterJoin(), groupByKey(), reduceByKey(), combineByKey() and lookup()) that benefits from partitioning.

* Too few partition won't take full advantage of your cluster.

* Too many partitions is too much overhead from shuffling data.

Spark has two types of partitioning techniques.

#### HashPartitioner
HashPartitioner works on Java’s Object.hashcode(). The concept of hashcode() is that objects which are equal should have the same hashcode. So based on this hashcode() concept *HashPartitioner will divide the keys that have the same hashcode()*.

It is the default partitioner of Spark. If we did not mention any partitioner then Spark will use this HashPartitioner for partitioning the data.

```Scala
scala> import org.apache.spark.HashPartitioner
import org.apache.spark.HashPartitioner

scala> val partitionCount = 10
partitionCount: Int = 10

scala> val partitioner = new HashPartitioner(partitionCount)
partitioner: org.apache.spark.HashPartitioner = org.apache.spark.HashPartitioner@a

scala> val rawWordsRDD = sc.textFile("data/countwords.txt")
rawWordsRDD: org.apache.spark.rdd.RDD[String] = data/countwords.txt MapPartitionsRDD[1] at textFile at <console>:26

scala> val counts = rawWordsRDD.flatMap(line => line.split("\\W+")).map(x => x.toLowerCase()).map(x => (x, 1)).partitionBy(partitioner)
counts: org.apache.spark.rdd.RDD[(String, Int)] = ShuffledRDD[5] at partitionBy at <console>:28

scala> counts.reduceByKey((x, y) => x + y).saveAsTextFile("data/partition_spark/hash")


## Now you will see partitioned RDD stored in text file under `data/partition_spark/hash` directory.
```

#### RangePartitioner
Range partition will divide the records almost in equal ranges. First, the RangePartitioner will sort the records based on the key and then it will divide the records into a number of partitions based on the given value.

```Scala
scala> import org.apache.spark.RangePartitioner

scala> val partitionCount = 10

scala> val partitioner = new RangePartitioner(partitionCount)

scala> val rawWordsRDD = sc.textFile("data/countwords.txt")

scala> val counts = rawWordsRDD.flatMap(line => line.split("\\W+")).map(x => x.toLowerCase()).map(x => (x, 1)).partitionBy(partitioner)

scala> counts.reduceByKey((x, y) => x + y).saveAsTextFile("data/partition_spark/range")

## Now you will see partitioned RDD stored in text file under `data/partition_spark/range` directory.
```

#### CustomPartitioner
We can also customize the number of partitions we need and what should be stored in those partitions by extending the default Partitioner class in Spark.

```Scala
import org.apache.spark.Partitioner

class CustomPartitioner(numParts: Int) extends Partitioner {

  override val numPartitions: Int = numParts

  override def getPartition(key: Any): Int = {
    val k: Int = key.asInstanceOf[Int]
    return k % numPartitions
  }

  override def equals(other: Any): Boolean = {
    other match {
      case test: CustomPartitioner => test.numPartitions == numPartitions
      case _ => false
    }
  }
}
```

* *numPartitions*: Int, it takes the number of partitions that needs to be created.

* *getPartition(key: Any)*: Int, this method will return the particular key to the specified partition ID which ranges from 0 to numPartitions-1 for a given key.

* *equals()*: normal java equality method used to compare two objects, this method will test your partitioner object against other objects of itself then it decides whether two of your RDDs are Partitioned in the same way or not.

### Caching / Persisting RDD's

Spark RDDs are lazily evaluated, and sometimes we may wish to use the same RDD multiple times. If we do this naively, Spark will recompute the RDD and all of its dependencies each time we call an action on the RDD. Any time you will perform more than one action on an RDD, you must cache it. Otherwise, spark might re-evaluate the entire RDD all over again.

To avoid computing an RDD multiple times, we can ask Spark to persist the data. When we ask Spark to persist an RDD, the nodes that compute the RDD store their partitions. If a node that has data persisted on it fails, Spark will recompute the lost partitions of the data when needed.

**cache()** - Cache the RDD in memory `StorageLevel.MEMORY_ONLY`.

**persist()** - Cache the RDD in disk.

RDD can be stored using a different storage level.

|   Storage Level	|  Meaning 	|
|:-:	|---	|
|MEMORY_ONLY|Store RDD as deserialized objects in the JVM. If the RDD does not fit in memory, some partitions will not be cached and will be recomputed on the fly each time they're needed. This is the default level.|
|MEMORY_AND_DISK|Store RDD as deserialized objects in the JVM. If the RDD does not fit in memory, store the partitions that don't fit on disk, and read them from there when they're needed.|
|MEMORY_ONLY_SER (Java and Scala)|Store RDD as serialized Java objects (one byte array per partition). This is generally more space-efficient than deserialized objects but more CPU-intensive to read.|
|MEMORY_AND_DISK_SER (Java and Scala)|Similar to MEMORY_AND_DISK but stores RDD as serialized objects.|
|DISK_ONLY|Store the RDD partitions only on disk.|
|MEMORY_ONLY_2, MEMORY_AND_DISK_2|Replicate each partition on two cluster nodes.|

```scala
  import org.apache.spark.storage.StorageLevel._
  RDD.persist(MEMORY_AND_DISK_SER)
```

*[NOTE] If you attempt to cache too much data to fit in memory, Spark will automatically evict old partitions using a Least Recently Used (LRU) cache policy.*

#### Which Storage Level to Choose?

* If your RDDs fit comfortably with the default storage level (MEMORY_ONLY), leave them that way. This is the most CPU-efficient option, allowing operations on the RDDs to run as fast as possible.

* If not, try using MEMORY_ONLY_SER and selecting a fast serialization library to make the objects much more space-efficient, but still reasonably fast to access.

* Don’t spill to disk unless datasets computation is expensive. Otherwise, recomputing a partition may be as fast as reading it from disk.
* Use the replicated storage levels if you want fast fault recovery.

Spark automatically monitors cache usage on each node and drops out old data partitions in a LRU fashion. If you would like to manually remove an RDD instead of waiting for it to fall out of the cache, use the ```RDD.unpersist()``` method.

---

### Running Spark on a Cluster using SPARK-SUBMIT

* Make sure there are no path to local file system used in your script!!
* Package up your Scala Project into a jar file.
* use `spark-submit` to execute your driver script.

```Shell
spark-submit --class <class object that contains main function> --jars <path to any dependencies> --files <files you want placed alongside your application> <your jar-file-containing-spark-program>
```

```shell
# Word count
spark-submit --class com.hsahu.spark.WordCountSorted apache-spark-scala.jar

# Item Based collaborative filtering
spark-submit --class com.hsahu.spark.advance.ItemBasedCollaborativeFiltering apache-spark-scala.jar 50
```

#### Other Parameters

**--master** - What kind of cluster you have ? e.g.
* **yarn** - for running a YARN / Hadoop cluster

* **hostname:port** - for connecting to a master on a spark standalone cluster

* A master in your SparkConf will override this!!

* Config Priority - *SparkConf (spark-script) > command-line > config file for spark*

**--num-executors** - Must set explicitly with YARN (2 by default)

**--executor-memory** - Manages how much memory to use in each executor node.

**--total-executor-cores** - How much core to use in each executor node.

### Best practice for running on a cluster

* Just use an empty default `SparkConf` in your driver. This way, we'll use command-line options you pass into `spark-submit` from your master node.

* If executors start failing, you may need to adjust the memory each executor has (from the master node of cluster).
  ```Shell
  spark-submit --executor-memory 1g ItemBasedCollaborativeFiltering.jar 260
  ```

----

### SparkSQL

Documentation: [Apache Spark SQL Documentation ](https://spark.apache.org/docs/latest/sql-programming-guide.html)

Spark SQL is a Spark module for structured data processing. Unlike the basic Spark RDD API, the interfaces provided by Spark SQL provide more information about the structure of both the data and the computation being performed. It extends RDD to a "DataFrame" Object.

#### DataFrames (RDD of row objects)
* It is conceptually equivalent to a table in a relational database because it is a Dataset organized into named columns but with richer optimizations.

* Has a schema leading to more efficient storage.

* Read and write to JSON, Hive.

* Communicate with JDBC.

* DataFrames can be constructed from a wide array of sources such as: structured data files, tables in Hive, external databases, or existing RDDs.

* DataFrame is really a DataSet of Row objects `Dataset[Row]`.

#### DataSets

* DataSet is set of structured Data. It can be wrapped within a given struct or type. DataSet[Person], DataSet[(String, Double)].

* DataFrames schema is inferred at runtime; but a DataSet can be inferred at compile time (faster detection of errors and better optimizations). We can run SQL queries on DataFrames.
* RDD's can be converted to DataSets with `.toDS()` method.

* DataSets are more efficient. They can be serialized very efficiently. Optimal execution plans can be determined at compile time.

* Documentation : [org.apache.spark.sql.Dataset](https://spark.apache.org/docs/latest/api/scala/index.html#org.apache.spark.sql.Dataset)

#### SparkSession
* The entry point to programming Spark with the Dataset and DataFrame API.

* You can get a SparkContext from this session and use it to issue SQL queries on your datasets.

* Stop session when you are done.

* Documentation : [org.apache.spark.sql.SparkSession](https://spark.apache.org/docs/latest/api/scala/index.html#org.apache.spark.sql.SparkSession)


---


### Documentations

[SparkConf](https://spark.apache.org/docs/latest/api/scala/index.html#org.apache.spark.SparkConf)

[SparkContext](https://spark.apache.org/docs/latest/api/scala/index.html#org.apache.spark.SparkContext)
