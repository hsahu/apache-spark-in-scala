/*
 * http://www.scala-lang.org/api/2.12.1/scala/PartialFunction.html
 * https://stackoverflow.com/questions/32085060/partial-functions-in-scala
 */

/**
 * A partial function of type PartialFunction[A, B] is a unary function
 * where the domain does not necessarily include all values of type A.
 * The function isDefinedAt allows to test dynamically if a value is in the domain of the function.
 */
object PartialFunctionScala {
  val isEven: PartialFunction[Int, String] = {
    case x if x % 2 == 0 => x + " is even"
  }                                               //> isEven  : PartialFunction[Int,String] = <function1>

  val isOdd: PartialFunction[Int, String] = {
    case x if x % 2 == 1 => x + " is odd"
  }                                               //> isOdd  : PartialFunction[Int,String] = <function1>

  val numList: List[Int] = List(1, 2, 3, 4, 5)    //> numList  : List[Int] = List(1, 2, 3, 4, 5)

  val resultList = numList.map(isEven orElse (isOdd))
                                                  //> resultList  : List[String] = List(1 is odd, 2 is even, 3 is odd, 4 is even, 
                                                  //| 5 is odd)


  println(resultList)                             //> List(1 is odd, 2 is even, 3 is odd, 4 is even, 5 is odd)
}
