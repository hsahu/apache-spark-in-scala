package com.hsahu.scala.worksheet

object scalaWorksheet_Functions {
  def addNum(x: Int, y: Int): Int = {
    return 2 * x + y
  }                                               //> addNum: (x: Int, y: Int)Int

  def cubeIt(x: Int): Int = { x * x * x }         //> cubeIt: (x: Int)Int

  println(addNum(2, 3))                           //> 7
  println(addNum(y = 3, x = 2))                   //> 7
  println(cubeIt(2))                              //> 8
  println(cubeIt(x = 3))                          //> 27

  def transformInt(x: Int, f: Int => Int): Double = f(x) / 2.0
                                                  //> transformInt: (x: Int, f: Int => Int)Double

  println(transformInt(3, cubeIt))                //> 13.5

  // Lambda functions (anonymous function, function literals, inline function)
  println(transformInt(3, x => 2 * x * x * x))    //> 27.0
  println(transformInt(3, x => 2 * x))            //> 3.0
  println(transformInt(3, x => { val y = x * 2; y * y }))
                                                  //> 18.0
}