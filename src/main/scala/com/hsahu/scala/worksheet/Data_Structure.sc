package com.hsahu.scala.worksheet

object scalaWorkspace_DS {

	val number: String = "45"                 //> number  : String = 45
	val numberAsInt: Int = number.toInt       //> numberAsInt  : Int = 45
	println(numberAsInt)                      //> 45
	
  // Tuple (also common in spark) max 22 elements
  // Tuples -> immutable list
  // useful for passing around entire row of data
  // index start with one(1)

  val someData = ("Sahu", "1321", "hsahu")        //> someData  : (String, String, String) = (Sahu,1321,hsahu)
  println(someData)                               //> (Sahu,1321,hsahu)
  println(someData._1)                            //> Sahu
  println(someData._2)                            //> 1321
  println(someData._3)                            //> hsahu

  // you can mix multiple data types also
  var mixedData = ("hsahu", 23, true)             //> mixedData  : (String, Int, Boolean) = (hsahu,23,true)
  println(mixedData)                              //> (hsahu,23,true)
  println(mixedData._1)                           //> hsahu
  println(mixedData._2)                           //> 23
  println(mixedData._3)                           //> true

  // you can create key-value pair in scala
  var kvData = "Himanshu" -> "Walmart"            //> kvData  : (String, String) = (Himanshu,Walmart)
  println(kvData)                                 //> (Himanshu,Walmart)
  println(kvData._1 + " - " + kvData._2)          //> Himanshu - Walmart

  // nested key-value pairs
  var nkvData = 1 -> 2 -> 3 -> 4                  //> nkvData  : (((Int, Int), Int), Int) = (((1,2),3),4)
  println(nkvData)                                //> (((1,2),3),4)

  // Lists
  // it can't hold mixed data types
  // it's a singly linkedlist
  // index start with zero(0)
  val list = List("A", "B", "C")                  //> list  : List[String] = List(A, B, C)
  println(list)                                   //> List(A, B, C)
  println(list(0))                                //> A
  println(list(0) + list(1))                      //> AB
  // head and tail will give you first and remaining elements respectively
  // head is single element and tail itself is a List so you can perform all the List operation with tail
  println(list.head)                              //> A
  println(list.tail)                              //> List(B, C)
  println(list.tail.head)                         //> B
  println(list.tail.tail)                         //> List(C)
  println(list.tail.tail.head)                    //> C
  println(list.tail.tail.tail)                    //> List()

  // iterate through a list
  for (item <- list) println(item)                //> A
                                                  //| B
                                                  //| C

  // map() can be used to apply any method on elvery element
  list.map((item: String) => { item.toLowerCase })//> res0: List[String] = List(a, b, c)

  // reduce() can be used to combine togather all the items in a collection using some function
  val contatString = list.reduce((x: String, y: String) => x + y)
                                                  //> contatString  : String = ABC

  // filter() can remove element you don't want and return a new list
  val iHateB = list.filter { x => x != "B" }      //> iHateB  : List[String] = List(A, C)
  val iHateC = list.filter { _ != "C" }           //> iHateC  : List[String] = List(A, B)

  // Note thta Spark has its own map, reduce and filter functions that can distribute these operations but they work the same way

}