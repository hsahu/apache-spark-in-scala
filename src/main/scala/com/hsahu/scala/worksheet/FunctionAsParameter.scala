import java.io.File
import java.io.PrintWriter

object FunctionAsParameter extends App {
  /**
   * 1. ex - passing function as an argument
   */
  def func(op: (Double, Double) => Double, x: Double, y: Double) = op(x, y)

  println(func((x: Double, y: Double) => x + y, 5, 6))
  println(func(_ + _, 5, 6))

  /**
   * 2. ex - passing function as an argument
   * LOAN PATTERN
   */
  def withPrintWriter(file: File)(op: PrintWriter => Unit) = {
    val writer = new PrintWriter(file)
    try {
      op(writer)
    } finally {
      writer.close()
    }
  }

  val file = new File("file.txt")
  withPrintWriter(file)(writer => writer.println(new java.util.Date))
}
