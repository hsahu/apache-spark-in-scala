/**
 * Implicit Conversions are a set of methods that Scala tries to apply
 * when it encounters an object of the wrong type being used.
 */
object RationalExample extends App {

  var a: Rational = new Rational(3, 2)

  implicit def IntToRational(x: Int) = new Rational(x)
  /**
   * To perform Int.+(Rational) define implicit conversion
   */
  println(2 + a)
  implicit def RationalToDouble(x: Rational) = x.eval()
  /**
   * To perform Double.+(Rational) define implicit conversion
   */
  println(4.6 + a)
}
