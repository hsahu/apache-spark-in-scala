import java.net.MalformedURLException
import java.net.URL
import scala.io.Source
import scala.io.BufferedSource
import scala.util.control.Exception.Catch
import java.io.FileNotFoundException

object FileMatcher extends App {

  val someFunction = (_: String).endsWith(_: String)

  private def filesHere = (new java.io.File(".")).listFiles

  private def filesMatching(matcher: (String) => Boolean) =
    for (file <- filesHere; if matcher(file.getName))
      yield file

  def filesEnding(query: String) =
    filesMatching(_.endsWith(query))

  def filesContaining(query: String) =
    filesMatching(_.contains(query))

  def filesRegex(query: String) =
    filesMatching(_.matches(query))
}

object Functions extends App {

  val multiplierConstant = 10

  /**
   * create URL object from a path
   */
  def urlFor(path: String): URL = {
    try {
      new URL(path)
    } catch {
      case e: MalformedURLException =>
        new URL("http://www.scala-lang.org")
    }
  }

  /**
   * read from file function
   */
  def readFromFile(fileName: String): Unit = {

    var source: BufferedSource = null

    // first order function
    val printline = (line: String) => println(line)

    // inner function
    def readFile() = {
      try {
        source = Source.fromFile(fileName)
      } catch {
        case fnfe: FileNotFoundException =>
          println(s"[ERROR] file : ${fileName} not found.")
          throw fnfe
        case ex: Exception =>
          println("[ERROR] Unknow error")
          throw ex
      }
    }
    readFile()
    for (line <- source.getLines()) {
      printline(line)
    }
  }

  /**
   * Partially Applied function
   */
  def partiallyAppliedFunctionExample = {
    val sum = (a: Int, b: Int, c: Int) => a + b + c // fully applied function
    val sumPartially = sum(1, 2, _: Int) // partially applied function

    sum(1, 2, 3)
    sumPartially(4)
  }

  /**
   * A closure is a function, whose return value depends on the value of variables declared outside this function.
   *
   * The function value (the object) that's created at runtime from this function literal is called a closure.
   *
   * The name arises from the act of "closing" the function literal by "capturing" the bindings of its free variables.
   * A function literal with no free variables, such as (x: Int) => x + 1, is called a closed term.
   * Thus a function value created at runtime from this function literal is not a closure in the strictest sense,
   * because (x: Int) => x + 1 is already closed as written.
   * But any function literal with free variables, such as(x: Int) => x + more, is an open term.
   * Changes made by a closure to a captured variable are visible outside the closure.
   * If we change more variable it will be reflected outside the scope of the closure.
   *
   * closure may or may not be pure function.
   */
  def closureMultiplier(multiplier: Int): Int = {
    multiplier * multiplierConstant
  }

  /**
   * repeated parameters
   */
  def echo(args: String*) = for (arg <- args) println(arg)

  echo()
  echo("Himanshu")
  echo("Hello", "world")

  /**
   * yield <- You want to create a new collection from an existing collection by transforming the elements with an algorithm.
   */
  val yieldExample = for (i <- 1 to 5) yield (i, s" Number is: ${i.toString()}")

  /**
   * yield with condition
   */
  val yieldWithCondition = for (i <- 1 to 20; if (i % 3 == 0 && i % 2 == 0)) yield (i)

  println(yieldExample)
  println(yieldWithCondition)
}
