package com.hsahu.scala.worksheet

/**
 * Case classes are like regular classes with a few key differences which we will go over.
 * Case classes are good for modeling immutable data.
 */
object worksheet_case_class {

  case class Message(sender: String, recipient: String, body: String)

  val message2 = Message("jorge@catalonia.es", "guillaume@quebec.ca", "Com va?")
                                                  //> message2  : com.hsahu.scala.worksheet.worksheet_case_class.Message = Message
                                                  //| (jorge@catalonia.es,guillaume@quebec.ca,Com va?)
  val message3 = Message("jorge@catalonia.es", "guillaume@quebec.ca", "Com va?")
                                                  //> message3  : com.hsahu.scala.worksheet.worksheet_case_class.Message = Message
                                                  //| (jorge@catalonia.es,guillaume@quebec.ca,Com va?)

  /**
   * Case classes are compared by structure and not by reference
   * Even though message2 and message3 refer to different objects, the value of each object is equal.
   */
  val messagesAreTheSame = message2 == message3   //> messagesAreTheSame  : Boolean = true
}