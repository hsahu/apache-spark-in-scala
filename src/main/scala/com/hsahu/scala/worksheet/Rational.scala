/**
 * Rational Number class
 */
class Rational(n: Int, d: Int) {

  private val gc = gcd(n, d)
  val numerator = n / gc
  val denominator = d / gc

  if (d == 0) {
    throw new ZeroDenominatorException("Denominator can not be zero in rational number")
  } else {
    println("Rational: " + toString())
  }

  def this(numerator: Int) = this(numerator, 1)

  def eval(): Double = {
    numerator.toDouble / denominator.toDouble
  }

  def +(that: Rational): Rational = {
    new Rational(numerator * that.denominator + denominator * that.numerator, denominator * that.denominator)
  }

  def -(that: Rational): Rational = {
    new Rational(numerator * that.denominator - denominator * that.numerator, denominator * that.denominator)
  }

  def *(that: Rational): Rational = {
    new Rational(numerator * that.numerator, denominator * that.denominator)
  }

  def /(that: Rational): Rational = {
    new Rational(numerator * that.denominator, denominator * that.numerator)
  }

  def ==(that: Rational): Boolean = numerator == that.numerator && denominator == that.denominator

  def >(that: Rational): Boolean = numerator * that.denominator > denominator * that.numerator

  def !=(that: Rational): Boolean = !(==(that))

  def >=(that: Rational): Boolean = >(that) || ==(that)

  def <(that: Rational): Boolean = !(>(that)) && !=(that)

  def <=(that: Rational): Boolean = <(that) || ==(that)

  def +(that: Int): Rational = {
    new Rational(numerator + denominator * that, denominator)
  }

  def -(that: Int): Rational = {
    new Rational(numerator - denominator * that, denominator)
  }

  def *(that: Int): Rational = {
    new Rational(numerator * that, denominator)
  }

  def /(that: Int): Rational = {
    new Rational(numerator, denominator * that)
  }

  def ==(that: Int): Boolean = numerator == that * denominator

  def >(that: Int): Boolean = numerator > that * denominator

  def !=(that: Int): Boolean = !(==(that))

  def <(that: Int): Boolean = !(>(that)) && !=(that)

  def <=(that: Int): Boolean = <(that) || ==(that)

  def >=(that: Int): Boolean = >(that) || ==(that)

  private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)

  override def toString = numerator + "/" + denominator
}

/**
 * Exception
 */
case class ZeroDenominatorException(
  private val message: String    = "",
  private val cause:   Throwable = None.orNull)
  extends Exception(message, cause) 
