package com.hsahu.scala.worksheet

object scalaWorkspaceControlFlow {

  // if else
  if (1 > 3) {
    println("impossible")
  } else if (1 == 3) {
    println("it's a bug")
  } else {
    println("it's true")
  }                                               //> it's true

  // if variable doesn't match to anycase then it gonna throw expection if there is not any default case. so default case is recommended
  val number = 3                                  //> number  : Int = 3
  number match {
    case 1 => println("One")
    case 2 => println("Two")
    case 3 => println("Three")
    case 4 => println("Four")
    case _ => println("default")
  }                                               //> Three

  // For loops
  for (x <- 1 to 2) {
    val square = x * x
    println(square)
  }                                               //> 1
                                                  //| 4

  // while and do-while loop are same as java

  // scala returns final value automatically from a block
  { val x = 10; x + 30 }                          //> res0: Int = 40
  { var y = 10; y * 10 }                          //> res1: Int = 100
  println({ val x = 10; x + 30 })                 //> 40

}
