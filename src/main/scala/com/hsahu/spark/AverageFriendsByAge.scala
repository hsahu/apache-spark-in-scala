package com.hsahu.spark

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._

object AverageFriendsByAge {

  def parseAgeFriendsnumTupleFromLine(line: String) = {
    val fields = line.split(",")
    (fields(2).toInt, fields(3).toInt)
  }

  def main(args: Array[String]) {
    /**
     * Set the log level to only print errors
     */
    Logger.getLogger("org").setLevel(Level.ERROR)

    /**
     * Initialize Spark Context
     * Create a SparkContext using every core of the local machine, named AverageFriendsByAge
     * cluster name & job name
     *
     */
    val sparkContext = new SparkContext("local[*]", "AverageFriendsByAge")

    /**
     * create a key/value RDD by reading file and by parsing each line
     * RDD will contain (age, numFriends) tuple
     */
    val fakeFriendsRDD = sparkContext.textFile("data/fakefriends.csv").map(parseAgeFriendsnumTupleFromLine)

    /**
     * Now we have (age, numFriends) tuple
     * first create a temporary RDD (age, (numFriends, 1))
     * then reduce all the values of same age to create a RDD (age, (totalFriends, totalInstances)) and save this RDD
     */
    val totalsByAgeRDD = fakeFriendsRDD.mapValues(x => (x, 1)).reduceByKey((x, y) => (x._1 + y._1, x._2 + y._2))

    /**
     * Now we have tuple (age, (totalFriends, totalInstances))
     * Calculate average number of friends = (totalFriends / totalInstances) and create a new RDD
     */
    val averageByAgeRDD = totalsByAgeRDD.mapValues(x => x._1 / x._2)

    /**
     * Collect the results from the RDD
     */
    val result = averageByAgeRDD.collect()

    /**
     * Print age and average number of friends
     */
    result.sorted.foreach(println)

    /**
     * Stop spark Context
     */
    sparkContext.stop()
  }
}