package com.hsahu.spark

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._

/**
 * @author ${user.name}
 */
/**
 * Count up how many each star rating exists in the MovieLens 100K data set
 */
object RatingCounter {
  def main(args: Array[String]) {

    /**
     * Set the log level to only print errors
     */
    Logger.getLogger("org").setLevel(Level.ERROR)

    /**
     * Initialize Spark Context
     * Create a SparkContext using every core of the local machine, named RatingsCounter
     * cluster name & job name
     *
     */
    val sparkContext = new SparkContext("local[*]", "RatingsCounter")

    /**
     * STAGE 1
     * create RDD
     * Load up each line of the ratings data into an RDD
     */
    val lines = sparkContext.textFile("data/u.data")

    /**
     * STAGE 1
     * create new RDD by transforming original raw RDD
     * Convert each line to a string, split it out by tabs, and extract the third field.
     * (The file format is userID, movieID, rating, time stamp)
     */
    val ratings = lines.map(x => x.toString().split("\t")(2))

    /**
     * STAGE 2
     * RDD Action - countByValue
     * Count up how many times each value (rating) occurs
     */
    val results = ratings.countByValue()

    /**
     * Sort the resulting map of (rating, count) tuples
     */
    val sortedResults = results.toSeq.sortBy(_._1)

    /**
     * Print each result on its own line
     */
    sortedResults.foreach(println)

    /**
     * Stop spark Context
     */
    sparkContext.stop()
  }
}
