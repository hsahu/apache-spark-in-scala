package com.hsahu.spark.sparkSQL

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.spark.rdd._
import org.apache.spark.sql._
import org.apache.log4j._

object SparkSQL {

  /*these will be named column on sparkSQL*/
  case class Person(ID: Int, name: String, age: Int, numFriends: Int)

  def mapper(line: String): Person = {
    val fields: Array[String] = line.split(",")
    val person: Person = Person(fields(0).toInt, fields(1), fields(2).toInt, fields(3).toInt)
    return person
  }

  def main(args: Array[String]) {

    Logger.getLogger("org").setLevel(Level.ERROR)

    val sparkSession: SparkSession = SparkSession.builder().appName("SparkSQL").master("local[*]").getOrCreate();

    val peopleRDD: RDD[Person] = sparkSession.sparkContext.textFile("data/fakefriends.csv").map(mapper)

    /*Infer the schema and register the DataSet as a table.*/
    import sparkSession.implicits._

    val schemaPeople: Dataset[Person] = peopleRDD.toDS() // convert to DataSet

    schemaPeople.printSchema()

    /* Creates a new temporary table/view using a SparkDataFrame in the Spark Session.
     * If a temporary view with the same name already exists, replaces it.*/
    schemaPeople.createOrReplaceTempView("People")

    /*run SQL queries over DataFrames that have distributed as a table*/
    val teenagers: DataFrame = sparkSession.sql("SELECT * FROM people WHERE age >= 13 and age <= 19")

    val result: Array[Row] = teenagers.collect()

    result.foreach(println)

    sparkSession.stop()
  }
}