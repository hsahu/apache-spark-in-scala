package com.hsahu.spark.sparkSQL

import org.apache.spark._
import scala.io.Source
import scala.io.Codec
import java.nio.charset.CodingErrorAction
import org.apache.spark.sql._
import org.apache.log4j._
import org.apache.spark.sql.functions._

object MostPopularMovie {

  final case class Movie(movieID: Int)

  /**
   * Load up a Map of movie IDs to movie ratings
   */
  def loadMovieNames(): Map[Int, String] = {

    /**
     * Handle character encoding issue
     * A codec, is a way to decode a stored text file , text files can be encoded with different techniques,
     * so here as we have movie names with foreign characters, we are implicitly forcing our code to use the standard encoding (UTF-8) and replace foreign characters.
     *
     * implicit is a Scala way to create a variable that may get a value from the running context but if no value found it will use the implicit value.
     * In this case we want a codec to deal with special characters , it may be available in context and if not a one will be created with UTF-8.
     */
    implicit val codec = Codec("UTF-8")
    codec.onMalformedInput(CodingErrorAction.REPLACE)
    codec.onUnmappableCharacter(CodingErrorAction.REPLACE)

    /**
     * Create a Map of Int -> String , and get it from u.item
     */
    var movieNames: Map[Int, String] = Map()

    val lines = Source.fromFile("data/u.item").getLines()

    for (line <- lines) {
      var fields = line.split('|')
      if (fields.length > 1) {
        movieNames += (fields(0).toInt -> fields(1))
      }
    }

    return movieNames
  }

  def main(args: Array[String]) {

    Logger.getLogger("org").setLevel(Level.ERROR)

    val sparkSession: SparkSession = SparkSession.builder().appName("SparkSQL").master("local[*]").getOrCreate();

    val lines = sparkSession.sparkContext.textFile("data/u.data").map(x => Movie(x.split("\t")(1).toInt))

    // convert to data set
    import sparkSession.implicits._
    val movieDataset = lines.toDS()

    val topMovieIds = movieDataset.groupBy("movieID").count().orderBy(desc("count")).cache()

    topMovieIds.show()

    val top10 = topMovieIds.take(10)

    val name = loadMovieNames()

    println("Top 10 most popular movies :")
    for (result <- top10) {
      println(name(result(0).asInstanceOf[Int]) + " -> " + result(1))
    }
    sparkSession.stop()
  }
}