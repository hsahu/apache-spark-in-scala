package com.hsahu.spark.advance

import org.apache.spark.Partitioner

class CustomPartitioner(numParts: Int) extends Partitioner {

  override val numPartitions: Int = numParts

  override def getPartition(key: Any): Int = {
    val k: Int = key.asInstanceOf[Int]
    return k % numPartitions
  }

  override def equals(other: Any): Boolean = {
    other match {
      case test: CustomPartitioner => test.numPartitions == numPartitions
      case _ => false
    }
  }
}
