package com.hsahu.spark.advance

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._
import scala.io.Source
import scala.io.Codec
import java.nio.charset.CodingErrorAction

object MostPopularMovie {

  /**
   * Load up a Map of movie IDs to movie ratings
   */
  def loadMovieNames(): Map[Int, String] = {

    /**
     * Handle character encoding issue
     * A codec, is a way to decode a stored text file , text files can be encoded with different techniques,
     * so here as we have movie names with foreign characters, we are implicitly forcing our code to use the standard encoding (UTF-8) and replace foreign characters.
     *
     * implicit is a Scala way to create a variable that may get a value from the running context but if no value found it will use the implicit value.
     * In this case we want a codec to deal with special characters , it may be available in context and if not a one will be created with UTF-8.
     */
    implicit val codec = Codec("UTF-8")
    codec.onMalformedInput(CodingErrorAction.REPLACE)
    codec.onUnmappableCharacter(CodingErrorAction.REPLACE)

    /**
     * Create a Map of Int -> String , and get it from u.item
     */
    var movieNames: Map[Int, String] = Map()

    val lines = Source.fromFile("data/u.item").getLines()

    for (line <- lines) {
      var fields = line.split('|')
      if (fields.length > 1) {
        movieNames += (fields(0).toInt -> fields(1))
      }
    }

    return movieNames
  }

  def main(args: Array[String]) {

    Logger.getLogger("org").setLevel(Level.ERROR)

    val sparkContext = new SparkContext("local[*]", "MostPopularMovie")

    val lines = sparkContext.textFile("data/u.data")

    // Create a broadcast variable of our ID -> movie name Map
    var nameDict = sparkContext.broadcast(loadMovieNames)

    val movies = lines.map(x => (x.split("\t")(1).toInt, 1))

    val moviesCount = movies.reduceByKey((x, y) => x + y)

    val sortedMovies = moviesCount.map(x => (x._2, x._1)).sortByKey(false)

    val sortedMoviesWithNames = sortedMovies.map(x => (nameDict.value(x._2), x._1))

    println("Top 10 most popular movies :")

    sortedMoviesWithNames.take(10).foreach(println)

    sparkContext.stop()
  }
}