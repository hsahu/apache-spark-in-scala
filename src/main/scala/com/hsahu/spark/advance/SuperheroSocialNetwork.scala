package com.hsahu.spark.advance

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._

object SuperheroSocialNetwork {

  /*Function to extract heroID -> hero name tuple (or none in case of failure)*/
  def parseNames(line: String): Option[(Int, String)] = {
    val fields = line.split('\"')
    if (fields.length > 1) {
      return Some(fields(0).trim().toInt, fields(1))
    } else {
      return None /*flatmap will discard None value*/
    }
  }

  /*Convert to heroID -> number of connections data*/
  def countCoOccurences(line: String) = {
    val fields = line.split("\\s+")
    (fields(0).trim().toInt, fields.length - 1)
  }

  def main(args: Array[String]) {

    Logger.getLogger("org").setLevel(Level.ERROR)

    val sparkContext = new SparkContext("local[*]", "SuperheroSocialNetwork")

    /*Creating RDD is not required you can broadcast it also*/
    val namesRDD = sparkContext.textFile("data/Marvel-names.txt").flatMap(parseNames)

    val graphRDD = sparkContext.textFile("data/Marvel-graph.txt").map(countCoOccurences)

    val totalFriendsByCharacter = graphRDD.reduceByKey((x, y) => x + y)

    val flipped = totalFriendsByCharacter.map(x => (x._2, x._1))

    val mostPopular = flipped.max()

    val mostPopularName = namesRDD.lookup(mostPopular._2)(0)

    println("Most Popular SuperHero : " + mostPopularName)

    sparkContext.stop()
  }
}