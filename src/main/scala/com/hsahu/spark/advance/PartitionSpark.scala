package com.hsahu.spark.advance

import org.apache.spark.HashPartitioner
import org.apache.spark.SparkContext
import org.apache.spark.RangePartitioner

object PartitionSpark {
  def main(args: Array[String]) {

    val sparkContext = new SparkContext("local[*]", "PartitionScala")

    val partitionCount = 10

    val hashPartitioner = new HashPartitioner(partitionCount)

    val rawWordsRDD = sparkContext.textFile("data/countwords.txt")

    val counts = rawWordsRDD.flatMap(line => line.split("\\W+")).map(x => x.toLowerCase()).map(x => (x, 1)).partitionBy(hashPartitioner)

    counts.reduceByKey((x, y) => x + y).saveAsTextFile("data/partition_spark/hash")

    sparkContext.stop()
  }
}
