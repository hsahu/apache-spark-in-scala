package com.hsahu.spark.sparkStreaming.twitter

import org.apache.spark._
import org.apache.spark.streaming._
import org.apache.spark.streaming.twitter._
import Utils._
import org.apache.spark.streaming.dstream.ReceiverInputDStream
import org.apache.spark.streaming.dstream.DStream

object PrintTweets {

  def main(args: Array[String]) {

    /**
     * setup twitter credentials
     */
    setupTwitter()

    /**
     * create spark configuration object
     */
    val sparkConf: SparkConf = new SparkConf().setAppName("PrintTweets").setMaster("local[3]")

    /**
     * setup stark streaming context named "PrintTweets" that runs locally using 3 CPU cores and one-second batches of data
     */
    val streamingContext: StreamingContext = new StreamingContext(sparkConf, Seconds(1))

    /**
     * setup error logging level
     */
    setupLogging()

    /**
     * create a input receiver DStream from twitter using spark streaming context
     * this will receive the raw information from twitter in a DStream
     */
    val tweets: ReceiverInputDStream[twitter4j.Status] = TwitterUtils.createStream(streamingContext, None)

    /**
     * Now extract the text of each status update into RDD"s
     */
    val statuses: DStream[String] = tweets.map(status => status.getText())

    /**
     * print out the first 10 entries
     */
    statuses.print(10)

    /**
     * start spark streaming context
     */
    streamingContext.start()

    /**
     * wait for the spark context to terminate
     */
    streamingContext.awaitTermination()
  }
}