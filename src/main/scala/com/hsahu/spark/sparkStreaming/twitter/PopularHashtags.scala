package com.hsahu.spark.sparkStreaming.twitter

import org.apache.spark._
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.Seconds
import org.apache.spark.streaming.twitter.TwitterUtils
import org.apache.spark.streaming.Duration
import scala.util.matching.Regex
import Utils._

object PopularHashtags {

  /**
   * process data of last 300 seconds
   */
  val windowDuration: Duration = Seconds(300)

  /**
   * slide window in every 6 seconds
   */
  val slideDuration: Duration = Seconds(6)

  /**
   * To fetch all hashTags irrespective of language
   */
  val regex: Regex = new Regex(".*")

  /**
   * To fetch hashTags written in english only
   */
  val regexEnglish: Regex = new Regex("[0-9#@_/-a-zA-Z]+")

  def main(args: Array[String]) {

    /**
     * setup twitter credentials
     */
    setupTwitter()

    /**
     * create spark configuration object
     */
    val sparkConf = new SparkConf().setAppName("PopularHashtags").setMaster("local[*]")

    /**
     * setup stark streaming context named "PrintTweets" that runs locally using all CPU cores and one-second batches of data
     */
    val streamingContext = new StreamingContext(sparkConf, Seconds(2))

    /**
     * setup error logging level
     */
    setupLogging()

    /**
     * create a input receiver DStream from twitter using spark streaming context
     * this will receive the raw information from twitter in a DStream
     */
    val tweetsRecieverInputDStream = TwitterUtils.createStream(streamingContext, None)

    /**
     * Now extract the text of each status update into RDD"s
     */
    val tweets = tweetsRecieverInputDStream.map(status => status.getText)

    /**
     * split tweets to words
     */
    val tweetsWords = tweets.flatMap(tweet => tweet.split(" "))

    /**
     * filter only hashTags
     */
    val hashTags = tweetsWords.filter(tweetWord => { tweetWord.startsWith("#") && (tweetWord.matches(regex.toString())) })

    /**
     * transform rdd to (hashTag, 1)
     */
    val hashTagsKeyValuePair = hashTags.map(hashTag => (hashTag, 1))

    /**
     * reduce by window and key and count hashTag occurrence
     */
    val hashTagCountWindow = hashTagsKeyValuePair.reduceByKeyAndWindow((a: Int, b: Int) => a + b, windowDuration, slideDuration)

    /**
     * sort hashTags by count
     */
    val sortedhashTags = hashTagCountWindow.transform(rdd => rdd.sortBy(x => x._2, false))

    /**
     * print out the first 10 entries
     */
    sortedhashTags.print()

    /**
     * setup checkpoint directory
     */
    streamingContext.checkpoint("data/twitter/checkpoints")

    /**
     * start spark streaming context
     */
    streamingContext.start()

    /**
     * wait for the spark context to terminate
     */
    streamingContext.awaitTermination()

  }
}