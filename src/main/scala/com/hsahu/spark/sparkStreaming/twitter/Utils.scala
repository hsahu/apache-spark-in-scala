package com.hsahu.spark.sparkStreaming.twitter

import org.apache.log4j._
import scala.io.Source

object Utils {

  /**
   * Configure twitter credentials
   */
  def setupTwitter() {
    Source.fromFile("data/twitter/twitter.properties").getLines().foreach(line => {
      val fields = line.split(" ")
      System.setProperty("twitter4j.oauth." + fields(0), fields(1))
    })
  }

  def setupLogging() {
    Logger.getRootLogger.setLevel(Level.ERROR)
  }
}