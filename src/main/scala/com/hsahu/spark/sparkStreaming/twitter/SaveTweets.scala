package com.hsahu.spark.sparkStreaming.twitter

import org.apache.spark.SparkConf
import org.apache.spark.streaming._
import org.apache.spark.streaming.dstream.{ ReceiverInputDStream, DStream }
import org.apache.spark.streaming.twitter.TwitterUtils
import Utils._
import org.apache.spark.rdd.RDD

/**
 * save tweets to disk
 */
object SaveTweets {

  val MAX_NUM_TWEETS: Long = 500

  def main(args: Array[String]) {
    /**
     * setup twitter credentials
     */
    setupTwitter()

    /**
     * create spark configuration object
     */
    val sparkConf: SparkConf = new SparkConf().setAppName("SaveTweets").setMaster("local[3]")

    /**
     * setup stark streaming context named "PrintTweets" that runs locally using 3 CPU cores and one-second batches of data
     */
    val streamingContext: StreamingContext = new StreamingContext(sparkConf, Seconds(1))

    /**
     * setup error logging level
     */
    setupLogging()

    /**
     * create a input receiver DStream from twitter using spark streaming context
     * this will receive the raw information from twitter in a DStream
     */
    val tweets: ReceiverInputDStream[twitter4j.Status] = TwitterUtils.createStream(streamingContext, None)

    /**
     * read tweets and create a DStream
     */
    val statues: DStream[String] = tweets.map(status => status.getText())

    /**
     * initialize and count number of tweets
     */
    var totalTweets: Long = 0

    statues.foreachRDD((rdd: RDD[String], time: Time) => {

      if (rdd.count() > 0) {
        /**
         * combine each partition's results into a single RDD
         */
        val repartitionedRDD = rdd.repartition(1).cache()

        /**
         * Save to disk
         */
        repartitionedRDD.saveAsTextFile("data/twitter/save-tweets/tweets_" + time.milliseconds.toString())

        totalTweets += repartitionedRDD.count()

        println("Total Tweets : " + totalTweets)

        if (totalTweets > MAX_NUM_TWEETS) {
          System.exit(0)
        }
      }
    });

    /**
     * set a checkpoint
     */
    streamingContext.checkpoint("data/twitter/checkpoints")

    /**
     * start spark streaming context
     */
    streamingContext.start()

    /**
     * wait for the spark context to terminate
     */
    streamingContext.awaitTermination()
  }
}