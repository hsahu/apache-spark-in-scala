package com.hsahu.spark.sparkStreaming.structuredstreaming

import org.apache.log4j._
import java.util.regex.Pattern
import org.apache.spark.sql.SparkSession
import java.text.SimpleDateFormat
import java.util.Locale
import java.util.regex.Matcher
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.streaming.Seconds
import org.apache.spark.streaming.Duration

object LogAnalysis {

  case class LogEntry(ip: String, client: String, user: String, dateTime: String, request: String, status: String, bytes: String, referer: String, agent: String)

  def apacheLogPattern(): Pattern = {
    val ddd = "\\d{1,3}"
    val ip = s"($ddd\\.$ddd\\.$ddd\\.$ddd)?"
    val client = "(\\S+)"
    val user = "(\\S+)"
    val dateTime = "(\\[.+?\\])"
    val request = "\"(.*?)\""
    val status = "(\\d{3})"
    val bytes = "(\\S+)"
    val referer = "\"(.*?)\""
    val agent = "\"(.*?)\""
    val regex = s"$ip $client $user $dateTime $request $status $bytes $referer $agent"
    Pattern.compile(regex)
  }

  val logPattern = apacheLogPattern()
  val datePattern = Pattern.compile("\\[(.*?) .+]")

  def setupLogging() {
    Logger.getLogger("org").setLevel(Level.ERROR)
  }

  def parseDateField(field: String): Option[String] = {
    val dateMatcher = datePattern.matcher(field)
    if (dateMatcher.find) {
      val dateString = dateMatcher.group(1)
      val dateFormat = new SimpleDateFormat("dd/MMM/yyyy:HH:mm:ss", Locale.ENGLISH)
      val date = (dateFormat.parse(dateString))
      val timestamp = new java.sql.Timestamp(date.getTime());
      return Option(timestamp.toString())
    } else {
      None
    }
  }

  // Convert a raw line of Apache access log data to a structured LogEntry object (or None if line is corrupt)
  def parseLog(x: Row): Option[LogEntry] = {

    val matcher: Matcher = logPattern.matcher(x.getString(0));
    if (matcher.matches()) {
      val timeString = matcher.group(4)
      return Some(LogEntry(
        matcher.group(1),
        matcher.group(2),
        matcher.group(3),
        parseDateField(matcher.group(4)).getOrElse(""),
        matcher.group(5),
        matcher.group(6),
        matcher.group(7),
        matcher.group(8),
        matcher.group(9)))
    } else {
      return None
    }
  }

  def main(args: Array[String]) {

    // Use new SparkSession interface in Spark 2.0
    val sparkSession = SparkSession.builder().appName("LogAnalysis").master("local[*]").getOrCreate()

    setupLogging()

    // Create a socket stream
    val rawData = sparkSession.readStream.format("socket").option("host", "localhost").option("port", 9999).load()

    // Must import spark.implicits for conversion to DataSet to work!
    import sparkSession.implicits._

    // Convert our raw text into a DataSet of LogEntry rows, then just select the two columns we care about
    val structuredData = rawData.flatMap(parseLog).select("status", "dateTime")

    // Group by status code, with a one-hour window.
    val windowed = structuredData.groupBy(new Column("status"), window(new Column("dateTime"), "1 minute")).count().orderBy("window")

    // Start the streaming query, dumping results to the console. Use "complete" output mode because we are aggregating
    // (instead of "append").
    val query = windowed.writeStream.outputMode("complete").format("console").start()

    // Keep going until we're stopped.
    query.awaitTermination()

    sparkSession.stop()
  }

}