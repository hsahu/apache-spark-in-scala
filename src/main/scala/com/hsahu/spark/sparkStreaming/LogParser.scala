package com.hsahu.spark.sparkStreaming

import org.apache.log4j._
import org.apache.spark.SparkConf
import org.apache.spark.streaming._
import org.apache.spark.storage.StorageLevel
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Real time log analysis using nc utility `netcat -l -p 9999`
 *
 */
object LogParser {

  /**
   * process data of last 300 seconds
   */
  val windowDuration: Duration = Seconds(300)

  /**
   * slide window in every 6 seconds
   */
  val slideDuration: Duration = Seconds(1)

  /**
   * get realtime logs from localhost socket
   */
  val hostName: String = "localhost"

  /**
   * get realtime logs from localhost socket at port
   */
  val port: Int = 9999

  /**
   * storage level of incoming data. Here we will store logs in both memory and disk in serializable format
   */
  val storageLevel: StorageLevel = StorageLevel.MEMORY_AND_DISK_SER

  /**
   * construct a regular expression to extract fields from raw Apache log lines
   */
  val pattern = apacheLogPattern()

  /**
   *  Retrieves a regex Pattern for parsing Apache access logs.
   */
  def apacheLogPattern(): Pattern = {
    val ddd = "\\d{1,3}"
    val ip = s"($ddd\\.$ddd\\.$ddd\\.$ddd)?"
    val client = "(\\S+)"
    val user = "(\\S+)"
    val dateTime = "(\\[.+?\\])"
    val request = "\"(.*?)\""
    val status = "(\\d{3})"
    val bytes = "(\\S+)"
    val referer = "\"(.*?)\""
    val agent = "\"(.*?)\""
    val regex = s"$ip $client $user $dateTime $request $status $bytes $referer $agent"
    Pattern.compile(regex)
  }

  def setupLogging() {
    Logger.getRootLogger.setLevel(Level.ERROR)
  }

  def main(args: Array[String]) {

    /**
     * create a spark configuration object which will use all cores of local host
     */
    val sparkConf: SparkConf = new SparkConf().setAppName("LogParser").setMaster("local[*]")

    /**
     * create a streaming context
     */
    val streamingContext: StreamingContext = new StreamingContext(sparkConf, Seconds(1))

    /**
     * setup logging to log only errors
     */
    setupLogging()

    /**
     * open a socket text stream which will read logs from given socket as text
     */
    val logsDStream = streamingContext.socketTextStream(hostName, port, storageLevel)

    /**
     * extract a request field from each line
     */
    val logs = logsDStream.map(x => {
      val matcher: Matcher = apacheLogPattern().matcher(x)
      if (matcher.matches()) {
        // fetch request field only
        matcher.group(5)
      }
    })

    /**
     * extract URL field from logs
     */
    val urls = logs.map(log => {
      val logFields = log.toString().split(" ")
      if (logFields.size == 3) {
        val url = logFields(1)
        url
      } else {
        "[ERROR]"
      }
    })

    val urlCounts = urls.map(url => (url, 1))

    /**
     * reduce by URL over a 5-minute window sliding every second
     */
    val urlCountsWindow = urlCounts.reduceByKeyAndWindow((x: Int, y: Int) => x + y, windowDuration, slideDuration)

    /**
     * sort the result
     */
    val sortedUrlCounts = urlCountsWindow.transform(rdd => rdd.sortBy(x => x._2, false))

    /**
     * print the result
     */
    sortedUrlCounts.print(10)

    /**
     * start a streaming context
     */
    streamingContext.start()

    /**
     * streamingContext will just waits for the termination signal from user for gracefully exit
     */
    streamingContext.awaitTermination()

  }
}
