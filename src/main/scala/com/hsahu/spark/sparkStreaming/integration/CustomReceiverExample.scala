package com.hsahu.spark.sparkStreaming.integration

import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.Seconds

/**
 * Networked Word Count example
 */
object CustomReceiverExample {

  val windowDuration = Seconds(300)

  val slideDuration = Seconds(2)

  def main(args: Array[String]) {

    val ssc = new StreamingContext("local[*]", "CustomReceiverExample", Seconds(1))

    org.apache.log4j.Logger.getRootLogger.setLevel(org.apache.log4j.Level.ERROR)

    val inputReadStream = ssc.receiverStream(new CustomReceiver("localhost", 9999))

    val lines = inputReadStream.flatMap(x => x.toLowerCase().split(" ")).map(x => (x, 1))

    val windowedWordsData = lines.reduceByKeyAndWindow((x: Int, y: Int) => x + y, windowDuration, slideDuration)

    val sortedWordCount = windowedWordsData.transform(rdd => rdd.sortBy(word => word._2, false))

    sortedWordCount.print()

    ssc.start()

    ssc.awaitTermination()
  }
}