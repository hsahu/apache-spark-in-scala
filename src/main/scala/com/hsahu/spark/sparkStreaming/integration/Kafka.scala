package com.hsahu.spark.sparkStreaming.integration

import org.apache.spark.streaming.StreamingContext
import org.apache.spark.SparkConf
import org.apache.spark.streaming.Seconds
import org.apache.spark.streaming.kafka._
import org.apache.log4j._
import kafka.serializer.StringDecoder
import org.apache.spark.streaming.dstream.InputDStream

object Kafka {

  def main(args: Array[String]) {

    /**
     * create a spark configuration object which will use all cores of local host
     */
    val sparkConf: SparkConf = new SparkConf().setAppName("Kafka Integration").setMaster("local[*]")

    /**
     * create a streaming context
     */
    val ssc = new StreamingContext(sparkConf, Seconds(1))

    /**
     * set logging level
     */
    Logger.getRootLogger.setLevel(Level.ERROR)

    // hostname:port for Kafka brokers, not Zookeeper
    val kafkaParams = Map("metadata.broker.list" -> "localhost:9092")

    // list of all the topics
    val topics = List("first_topic").toSet

    // Create our Kafka stream, which will contain (topic,message) pairs.
    val kafkaStream = KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](ssc, kafkaParams, topics)

    // only get the messages, which contain individual lines of data
    val lines = kafkaStream.map((x) => x._2)

    // print 
    lines.print()

    ssc.start()

    ssc.awaitTermination()
  }
}