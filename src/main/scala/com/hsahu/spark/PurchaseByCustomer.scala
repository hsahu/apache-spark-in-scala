package com.hsahu.spark

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._

object PurchaseByCustomer {

  def parseLine(line: String) = {
    /**
     * customerId | itemId | amountSpend
     */
    val fields = line.split(",")
    val customerId = fields(0)
    val amountSpend = fields(2).toFloat
    (customerId, amountSpend)
  }

  def main(args: Array[String]) {

    Logger.getLogger("org").setLevel(Level.ERROR)

    val sparkContext = new SparkContext("local[*]", "PurchaseByCustomer")

    val lines = sparkContext.textFile("data/customer-orders.csv")

    val parsedLines = lines.map(parseLine)

    val amountSpendByCustomer = parsedLines.reduceByKey((x, y) => x + y)

    val sortedAmountSpendByCustomer = amountSpendByCustomer.map(x => (x._2, x._1)).sortByKey(false).map(x => (x._2, x._1))

    for (result <- sortedAmountSpendByCustomer.collect()) {
      val customerId = result._1
      val amount = result._2
      println(s"$customerId - $amount")
    }

    sparkContext.stop()
  }
}