### SBT (Scala Build Tool) PROJECT

Best way to run scala project using sbt. sbt is similar to maven in java, it downloads all the dependency defined in [build.sbt](build.sbt) file and save the dependency under `target/` directory. It also generate a jar file for the application under `target/scala-*.**`.

To create a build run following command. This command is 

```Shell
sbt assembly
```

Default class that has to be run will be under /src/main/scala directory.
