#### Jar build by SBT

default class is under src/main/scala/ directory.


Running spark program using spark-submit


```Shell
spark-submit <jar-file-name>.jar <arguments>

```

Make sure to run your jar file at apropiate directory. If you are reading data from filesystem `data/u.item` then copy the jar to that folder which contains data folder 
and `data/u.item` file.

